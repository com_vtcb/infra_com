#!/usr/bin/env python
"""Reliable UDP based in python socket module.

Implements a reliability mechanism to prevent data loss and disorder.
This mechanism is similar to a simplified TCP.

The socket is divided between Sender and Receiver.
The Sender is responsible of receiving data from the application and sending it through socket.
The Receiver is responsible of receiving data from socket and sending it to application.

Reliability is kept by an acknowledgement and timeout protocol, preventing data loss, and by a sequence number protocol, preventing data disordering.

Sender functioning:
    When the application sends data to socket, the data will be split in chunks of maximum size PKT_SIZE.
    Each chunk is attached to it's sequence number and the packet is created following the format of Message class in Message module.
    Then the packet is put in a list of packets still not acknowledged.

    The sender is allowed to send a packet only if the window size is not greater than MAX_N.
    Whenever packets are acknowledged, new packets are available to be sent, if any.

    The sequence number of a packet is defined by the number of bytes of data preceeding the data of the packet.
    In other words, it increments for each packet by its length.

    The Sender keeps a timer.
    When a packet is transmitted, the timer will be started, if not on.
    When timer exceeds TIMEOUT, the oldest unacknowledged packet is retransmitted and the timer is reset.
    
    When a sequence number is acknowledged, all packets with sequence number less than the acknowledgement received are acknowledged, and deleted.
    Also, the window is updated, meaning other packet will be available to be sent (As explained above).

Receiver functioning:
    When the application requests for received data, all the received data without any gaps is returned.
    
    When the socket receives data, it is parsed and converted to Message format.
    The message is stored.
    Then is sent back an acknowledgement message for the greater sequence number possible so that all data before it was already received.
    The data available for the application is updated, so that it is reordered and does not contain any gaps.

TIMEOUT estimative:
    The TIMEOUT is dynamically estimated.
    Whenever a packet is transmitted for the first time, the time of its transmission is recorded.
    Whenever an acknowledgement is received for a sequence number for the first time, the TIMEOUT value is recalculated.
    The TIMEOUT estimative is given by:    
        sampleRTT       = time_elapsed_since_packet_was_sent_for_the_first_time
        estimatedRTT    = (1 - ALPHA) * estimatedRTT + ALPHA * sampleRTT
        deviationRTT    = (1 - BETA)  * deviationRTT + BETA  * abs(sampleRTT - estimatedRTT)
        TIMEOUT         = estimatedRTT + 4 * devationRTT
        
        Where ALPHA = 0.125 and BETA = 0.25
"""

from socket import socket, AF_INET, SOCK_DGRAM, error, timeout
from thread import start_new_thread as start
from time import time, sleep
from Queue import PriorityQueue
from Message import Message, stom

PKT_SIZE        = 1
MAX_N           = 5
TIMEOUT         = 0.05
ALPHA           = 0.125
BETA            = 0.25

def parseParenthesis(s):
    """Splits function 2 strings so that first one is the first expression separated by parenthesis.

    If it is impossible to split, returns None.
    """
    i = 0
    l = len(s)
    p = 0
    while i < l:
        if s[i] == '(':
            p += 1
        elif s[i] == ')':
            p -= 1
        
        i += 1
        
        if p == 0:
            return s[:i], s[i:]
    return None

class UDP:
    """Reliable UDP Socket"""
    def __init__(self, src = None, dst = None):
        """Returns new UDP object that handles a connection between src and dst

    _sock:      Socket object

    Socket functioning:
        running:    Threads stop condition
        retrans_on: Thread retransmitter is running
        receive_on: Thread receiver is running

        src:        Source address. It's the one the socket is bound
        dst:        Destination address. It's the one to send data

        PKT_SIZE:   Maximum size of a packet
                    Notice that it does not include extra information about the message
        MAX_N:      Maximum number of unACKed packets

    Sender:
        s_app:      List of unACKed packet.
        s_seq:      Sequence number counter
        s_ack:      Sequence number of last packet to be ACKed

        msgTimers:  Time each message was sent.
        
        window:     Number of unACKed packets

    Receiver:
        r_app:      Messages received no sent to app.
        r_pkt:      List of received packets
        r_seq:      Sequence number of last packet to be recieved
        
        
    TIMEOUT estimate:
        eRTT:       Estimated RTT
        dRTT:       Deviation of estimated RTT
        TIMEOUT:    Maximum interval of time to wait for ACK to be received
        
    TIMEOUT timer:
        timer:      Timer
        timer_on:   Timer is on"""

        self._sock      = socket(AF_INET, SOCK_DGRAM)

        self.running    = True
        self.retrans_on = False
        self.receive_on = False
        
        self.src        = src
        self.dst        = dst
        
        if src != None:
            self._sock.bind(src)
        
        self.PKT_SIZE   = PKT_SIZE
        self.MAX_N      = MAX_N
        
        self.s_app      = []
        self.s_seq      = 0
        self.s_ack      = 0

        self.msgTimers  = {}
        
        self.window     = 0

        self.r_app      = ""
        self.r_pkt      = []
        self.r_seq      = 0
        
        self.eRTT       = TIMEOUT
        self.dRTT       = 0
        self.TIMEOUT    = TIMEOUT

        self.timer      = 0
        self.timer_on   = False
        
        self._sock.settimeout(self.TIMEOUT)
        
        start( self.retransmitter, () )
        start( self.receiver, () )

    def status(self):
        """Prints object status"""
        print('>>> UDP STATUS! <<<')
        for i in filter(lambda aname: not aname.startswith('_'), dir(self)):
            if not hasattr(getattr(self, i), '__call__'):
                print '>>>>>>> ' + i + ': ', getattr(self, i)
        print('>>> Done! <<<')

    def __getattr__(self, attr):
        """Redirects any undefined attribute to _sock"""
        return getattr(self._sock, attr)
    
    def stopTimer(self):
        """Stops timer"""
        self.timer_on = False
    
    def setTimer(self, force = True):
        """Starts timer
    If force is False, only start timer if it is not on.
    Otherwise, resets timer."""
        if force or (not self.timer):
            self.timer_on = True
            self.timer = time()
    
    def timeout(self):
        """Returns wether time passed since the timer has started is greater than TIMEOUT"""
        return self.timer_on and (time() - self.timer > self.TIMEOUT)
    
    def transmit(self, pkt = 0):
        """Sends packet at index pkt

    If its not possible to trasmit (for example, there are no packets to be transmitted), return None
        """
        if self.s_app == []:
            return None
        
        try:
            print('Transmitting packet '+ str(self.s_app[pkt]) + '!')
            nxt_ack = self.s_app[pkt].seq + len(self.s_app[pkt])
            print self.s_app[pkt], len(self.s_app[pkt]), nxt_ack
            
            if nxt_ack not in self.msgTimers.keys():
                self.msgTimers[nxt_ack] = time()
                
            return self._sock.sendto( str(self.s_app[pkt]), self.dst )
        except:
            return None
        
    def updateWindow(self):
        """Sends unsent packets in available window"""
        for i in range(self.window, min(self.MAX_N, len(self.s_app))):
            self.transmit(i)
    
        self.window = min(self.MAX_N, len(self.s_app))

        self.setTimer(force=True)
    
    def send(self, msg):
        """Receives message and prepare it to be sent."""
        self.s_app += [ 
            Message (
                self.s_seq + i,
                msg[i:i + self.PKT_SIZE],
                False
            )
            for i in range(0, len(msg), self.PKT_SIZE)
        ]
        self.s_seq += len(msg)
    
        self.updateWindow()
        
        self.setTimer(force=False)
    
    def retransmitter(self):
        """Thread to resend packets when a timeout occur"""
        self.retrans_on = True
        while self.running:
            if self.timeout():
                #retransmit first pkt
                #print 'TIMEOUT'
                self.transmit()
                self.setTimer()
        
        self.retrans_on = False
    
    def s_updateACK(self):
        """Updates s_ack and delete already sent packets"""
        window_changed = False
        while len(self.s_app) > 0 and self.s_app[0].seq < self.s_ack:
            del self.s_app[0]
            self.window = max(self.window - 1, 0)
            window_changed = True

        if window_changed:
            self.updateWindow()

        if len(self.s_app) == 0:
            self.stopTimer()
    
    def r_updateACK(self):
        """Updates r_ack and r_msg"""
        while True:
            msg = [msg for msg in self.r_pkt if msg.seq == self.r_seq]
            if msg == []:
                break
            
            msg = msg[0]
            
            self.r_app += msg.msg            
            self.r_pkt = [msg for msg in self.r_pkt if msg.seq != self.r_seq]
            self.r_seq += len(msg)
    
    def sendACK(self):
        """Sends an ACK message"""
        return self._sock.sendto( str(Message(self.r_seq, '', True)), self.dst )
    
    def receiver(self):
        """Thread to receive packets"""
        self.receive_on = True
        pkt = ''
        while self.running:
            try:
                pkt += self._sock.recv(4096)
            except timeout:
                continue

            while True:
                # Get next packet received
                par = parseParenthesis(pkt)
                if par == None: break
                
                msg, pkt = par
                
                msg = stom(msg)
                
                if msg.ack:
                    # Ack received
                    print 'Received ACK:', msg
                    if self.s_ack < msg.seq:
                        # Update ACKed packets
                        self.s_ack = msg.seq

                        self.s_updateACK()
                        
                        # Update TIMEOUT
                        if msg.seq in self.msgTimers.keys():
                            self.estimateTIMEOUT(self.msgTimers[msg.seq])
                        self.msgTimers = {
                                s: t
                            for (s, t) in 
                                self.msgTimers.items()
                            if s > self.s_ack
                        }
                else:
                    # Message received
                    if msg.seq >= self.r_seq:
                        self.r_pkt = [i for i in self.r_pkt if i.seq != msg.seq]
                        self.r_pkt.append(msg)
                        self.r_updateACK();
                        print 'UPDATED'
                    self.sendACK()

        self.receive_on = False

    def estimateTIMEOUT(self, msgTime):
        """Estimates TIMEOUT"""
        sRTT = time() - msgTime
        self.eRTT    = (1 - ALPHA) * self.eRTT + ALPHA * sRTT
        self.dRTT    = (1 - BETA)  * self.dRTT + BETA  * abs(sRTT - self.eRTT)
        self.TIMEOUT = self.eRTT + 4 * self.dRTT
        self._sock.settimeout(self.TIMEOUT)
    
    def recv(self):
        """Returns last data received, if any"""
        msg = self.r_app
        self.r_app = ""
        return msg
    
    def close(self):
        """Close socket"""
        self.running = False
        
        while self.retrans_on: pass
        while self.receive_on: pass
        
        return self._sock.close()

class Application:
    """Application"""
    def menu(self):
        """Prints menu"""
        print('1 - Send message')
        print('2 - Received messages')
        print('3 - Status')
        print('4 - Exit')

    def inp(self):
        """Returns tuple (host, port) input"""
        return (raw_input('Enter host: '), int(raw_input('Enter port: ')))

    def main(self):
        """Application execution"""
        
        print 'Enter source data!'
        src = ('', int(raw_input('Enter port to bind: ')))
        print 'Enter destination data!'
        dst = self.inp()
        u = UDP( src, dst )
        
        exit = False
        
        while not exit:
            self.menu()
            op = raw_input('Option: ')
            
            if   op == '1':
                msg = raw_input('Enter message to send: ')
                u.send(msg)
            elif op == '2':
                msg = u.recv()
                if msg != "":
                    print "New data received:", msg
                else:
                    print "No new data received since last time."
            elif op == '3':
                u.status()
            elif op == '4':
                exit = True
                print('Bye! :D')
            else:
                print('Invalid Option!')

        u.close()

if __name__ == "__main__":
    Application().main()

