#!/usr/bin/env python
"""Message format.
"""

class Message:
    def __init__(self, seq, msg, ack):
        self.seq = seq
        self.msg = msg
        self.ack = ack
    
        self.__repr__ = self.__str__
    
    def __len__(self):
        return len(self.msg)

    def __str__(self):
        return str( (self.seq, self.msg, self.ack) )

def stom(s):
    return eval( 'Message' + s )

if __name__ == "__main__":
    print Message(1, 'm1', False)
    print str(Message(2, 'm2', False))
    print stom("(3, 'm3', False)")
    print stom(str(Message(4, '', True)))

