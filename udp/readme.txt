Reliable UDP socket based in python socket module.

Application:
    Exchange messages through two instances of the program.

Requirements:
    Python 2.7

Tested in:
    Ubuntu 12
    Python 2.7.3

Run:
    Run "python UDP.py"
    
Execution:
    First, the user should initialize the socket, entering the source and destination addresses.
    Then, the user will interact with a menu.
    To interact with the menu, the user should enter the number related to the option desired.
    Steps are detailed next.


    Initialize socket:
        Enter source port to bind.
        Enter destination (socket to be connected) host and port.

    Menu:
        There are 4 option in the menu:
        1 - Send message
            Waits for user to enter message to be sent through socket.
        2 - Received messages
            Prints messages received and still shown.
        3 - Status
            Prints socket status.
            Prints each attribute of socket being used and its value.
        4 - Exit
            Ends execution


