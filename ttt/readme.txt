Tic tac toe game peer-to-peer application.

Requirements:
    Python 2.7

Tested in:
    Ubuntu 12
    Python 2.7.3

Run:
    Run "python tic_tac_toe.py" for both the clients.

Execution:
    The application is p2p, so any client may connect to any other.
	First, enter your desired nickname. Then enter a port number to bind to.
	In the main menu, you can wait for another player to request connection, or you can try to connect to someone.
	
	If you wish to start the connection, select the first option.
	Then, just type in the IP adress, and then port number of the other player.
	The other player will receive a message and should press enter to show that you are active.
	Then, sending "y" will accept the offer and the game will start the match.
	If the one requested to play refuses it or takes too long to reply, the first one will be notified and can try again with the same or other player.
	
	In the game, you will have a dots, crosses and circles representation of the famous tic tac toe game.
	When it's your round, send the x and y position corresponding to the game matrix position to place your turn.
	One player is cross, the other is circle.
	When someone wins or its a draw, 
	If both players agree, they can play immediately again.
	If not, they will be sent to the main menu again.
	
	After a match, you can chose the second option from the menu to watch your match history.
	It will show the number of wins, draws and losses to each player you matched with.
	Choosing the third option from the main menu, you can leave the game application.
