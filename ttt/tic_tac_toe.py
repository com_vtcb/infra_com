# For sockets
import socket as dobby
# For threads
import thread as wool
# For system
from os import system

# Global variables.

# "in" Socket
# Waits for another player to connect.
sin			= None

# "out" Socket
# Tries to connect with other players.
sout		= None

# My nickname
# The history of games is kept by nicknames.
nickname	= None

# My host.
host		= ''

# My port (to bind the "in" socket).
port		= None

# Mutex 
playing		= wool.allocate_lock()

# Flag
justPlayed	= False

# History of games.
class History:
	def __init__(self):
		self.hist = {}

	# Adds another entry to history.
	def anotherFriend(self, boy):
		if not boy in self.hist:
			self.hist[boy] = [0, 0, 0]
	
	def update(self, boy, whoWonWhosNext):
		self.hist[boy][whoWonWhosNext] += 1
		
	def out(self):
		print 'History of games:'
		print '{0:<26}{1:<8}{2:<8}{3:<8}'.format(
			'nickname', 'losses', 'draws', 'wins'
		)
		for boy, result in self.hist.items():
			if result == (0, 0, 0):
				continue

			print '{0:<26}{1:<8}{2:<8}{3:<8}'.format(
				boy, result[0], result[1], result[2]
			)
		print '----------------------------------------------------'

history		= History();

# Tic Tac Toe
# The game itself.
class TicTacToe:
	# Initialize board.
	def __init__(self):
		self.board = []
		for i in range(3):
			self.board.append([])
			for j in range(3):
				self.board[i].append('.')

	# Check if x character wins.
	def win(self, x):
		for i in range(3):
			won = True
			for j in range(3):
				if self.board[i][j] != x:
					won = False
			if won:
				return True

		for i in range(3):
			won = True
			for j in range(3):
				if self.board[j][i] != x:
					won = False
			if won:
				return True

		won = True
		for i in range(3):
			if self.board[i][i] != x:
				won = False
		if won:
			return True

		won = True
		for i in range(3):
			if self.board[i][2 - i] != x:
				won = False
		if won:
			return True
		
		return False
	
	# Check if game ends with ye old lady.
	def draw(self):
		for i in self.board:
			for j in i:
				if j == '.':
					return False
		return True
	
	# Print the board.
	def printBoard(self):
		print('---------------')
		b = ''
		for i in self.board:
			for j in i:
				b += j
			b += '\n'
		print b[:-1]
		print('---------------')
	
	# Set position at the board.
	def set(self, x, y, c):
		self.board[x][y] = c
	
	def isSet(self, x, y):
		return self.board[x][y] != '.'

def clearScreen():
	return system('clear')

# The Big Bang
# First thing to be called.
# Initializes the "in" socket.
def bigBang():
	# Please, forgive us!
	global sin, port, nickname
	sin = dobby.socket(dobby.AF_INET, dobby.SOCK_STREAM)

	nickname	= raw_input('Enter your nickname: ')

	bound = False

	# Try to bind "in" socket to chosen port.
	while not bound:
		try:
			port		= int(raw_input('Enter port to bind: '))
			sin.bind( (host, port) )
			print 'Bound to port ' + str(port)
			bound = True
		except dobby.error as e:
			print 'Could not bind to port:'
			print str(e)
		except:
			print 'Could not bind to port:'
			print 'Unknown error'

# Connect
# Try to connect with another player.
def connect():
	# It's ok now...
	global sout
	sout = dobby.socket(dobby.AF_INET, dobby.SOCK_STREAM)

	# Try to connect to chosen host and port.
	try:
		host = raw_input('Enter IP to connect: ')
		port = int(raw_input('Enter port to connect: '))

		sout.connect( (host, port) )
		sout.send(nickname)
		
		# Wait for other player to answer.
		sout.settimeout(10)
		ans = sout.recv(1024)
		sout.settimeout(None)
		
		return (host, port), ans
	# Wait to much for response.
	except dobby.timeout:	
		return ('', -1), 'busy'
	# Error in connection.
	except dobby.error as e:
		return (str(e), -1), 'err'
	except:
		return ('Unknown error', -1), 'err'

def mainName():
	print '--------------------Ye Olde Lady--------------------'
	print 'You are ' + nickname + ' at ', (host, port)

# Menu
# Print menu
def menu():
	print '\t1 - Connect and play :D'
	print '\t2 - History :P'
	print '\t3 - Exit :('

# Get Move
# Read player move.
def getMove():
	try:
		x = int( raw_input('x position [0, 1, 2]: ') )
		y = int( raw_input('y position [0, 1, 2]: ') )
		return x, y
	except:
		return -1, -1

# Valid Move
# Check if position (x, y) is valid.
def validMove(x, y):
	return x >= 0 and x < 3 and y >= 0 and y < 3

# Play The Game
# Gameplay.
def playTheGame(s, turn):
	doneYet = False
	game = TicTacToe()
	if turn == True:
		mine = 'x'
		bomb = 'o'
	else:
		mine = 'o'
		bomb = 'x'

	while not doneYet:
		clearScreen()
		mainName()
		
		game.printBoard()
		
		if turn:	
			print 'It\'s your turn!'
		else:
			print 'Wait for your friend to play...'
		
		if turn:
			x, y = getMove()
			while not validMove(x, y) or game.isSet(x, y):
				print 'Invalid move! Try again :P'
				x, y = getMove()

			game.set(x, y, mine)
			s.send (
				str(x) + ' ' + str(y) + ' ' + str(mine)
			)
		else:
			x, y, c = [i for i in s.recv(1024).split()]
			
			game.set( int(x), int(y), c )
		
		turn = not turn
		doneYet = game.win(mine) or game.win(bomb) or game.draw()
	
	print 'Game!'
	clearScreen()
	mainName()
	game.printBoard()
	
	if game.win(mine):
		return 2
	elif game.win(bomb):
		return 0
	else: # draw()
		return 1

def letTheGamesBegin(s, boy, turn):
	doneYet = False
	while not doneYet:
		whoWonWhosNext = playTheGame(s, turn)
		history.update(boy, whoWonWhosNext)
		
		if(whoWonWhosNext == 0):
			result = 'gg, lixop. n000000000000b!!!'
		elif(whoWonWhosNext == 1):
			result = 'All heil the old woman!'
		elif(whoWonWhosNext == 2):
			result = 'That\'s right, baby!!! You are AWE-SOME!'
		
		print( result )
		
		acc = raw_input( 'Wanna play again? [y/something]: ' )
		
		ans = 'cebola'
		if acc == 'y':
			if turn:
				ans = s.recv(1024)
				s.send('ok')
			else:
				s.send('ok')
				ans = s.recv(1024)
		else:
			doneYet = True
		
		if ans != 'ok':
			doneYet = True
			
		turn = not turn

	try:
		s.send('fuck you')
	except:
		pass

def waitForIt():
	while True:
		sin.listen(1)
		cnx, add = sin.accept()
		
		if playing.locked():
			cnx.send('busy')
			cnx.close();
			continue
		
		clearScreen()
		mainName()
		
		print str(add) + ' is trying to connect...'
		print 'Hello..... Is there anybody in there.....? Just press enter if you can hear me.... Is there anyone at home........'

		playing.acquire()
		wool.interrupt_main() # gambiarra master
		otherguy = cnx.recv(1024)
		acc = raw_input( otherguy + ' asks: Do you wanna build a snowman? [y/anything]: ' )
		# Shuraisteioshuraigou?
		justPlayed = True

		try:
			if acc == 'y':
				cnx.send('ok')
				cnx.send(nickname)
				print 'I\'m in!'
			
				history.anotherFriend(otherguy)
				letTheGamesBegin(cnx, otherguy, False)
			else:
				cnx.send('jujubas')
				print 'Run, forrest, run!'
			cnx.close()
		except dobby.error as e:
			print 'Connection fucked up: ' + str(e)
		except:
			print 'Connection fucked up: Unknown error!'

		playing.release()

def aHasTooth():
	global justPlayed
	doneYet = False
	while not doneYet:
		if(playing.locked()):
			continue

		clearScreen()
		mainName()
		menu()
		
		try:
			opt = raw_input('Option: ')
		except:
			print('')
			continue;
		
		if justPlayed:
			justPlayed = False
			continue
		
		if opt == '1':
			playing.acquire()
			boy, ans = connect()

			if ans == 'ok':
				otherguy = sout.recv(1024)
				print( 'Yay, let\'s play! :D' )
				history.anotherFriend(otherguy)

				letTheGamesBegin(sout, otherguy, True)
			elif ans == 'busy':
				print 'No answer. Maybe your friend is busy in another game!'
			elif ans == 'err':
				print 'Couldn\'t connect...'
				print boy[0]
			else:
				print 'Dumb fuck didn\'t accept your challenge...'

			try:
				sout.close()
			except:
				pass

			playing.release()
		elif opt == '2':
			clearScreen()
			mainName()
			
			history.out()
		elif opt == '3':
			print 'Bye bye! Have a nice day! :)'
			doneYet = True
		else:
			print 'Not an option, dude... Be careful!'
		
		print 'Press enter to continue!'
		raw_input()

bigBang()
wool.start_new_thread( waitForIt, () )
aHasTooth()

sin.close()

