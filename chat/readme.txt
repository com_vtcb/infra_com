Chat application with server and clients, socket based in python socket module.

Requirements:
    Python 2.7

Tested in:
    Ubuntu 12
    Python 2.7.3

Run:
    Run "python Server.py" for the server and "python Client.py" for the clients.

Execution:
    Server:
        First, enter the desired port to be bound to the server.
        Then, the user will interact with a menu.
        To interact with the menu, the user should enter the number related to the option desired.

        Menu:
            There are 4 option in the menu:
            1 - Create room
                Creates a room, entering a name, and it will be added to the server's room list.
            2 - Print joke
                Print a brilliant pun.
            3 - Room status
                States which are the active rooms and all the clients connected to each one.
            4 - Ban IP
                Enter an IP adress to ban acess to the server for clients on that adress.
            5 - Unban IP
                Enter an IP adress to take the ban out of an IP.
            6 - Banned IP's
                Shows banned IP adresses.
            7 - Exit
                Ends execution.
    
    Client:
        The client interaction is intuitive using the GUI.
        Start by telling the application on the specified fields:
            - Desired nickname
            - IP of the server you want to connect to
            - Port of the server you want to connect to
        Then press the button to start connection.
        After that, a list of the available rooms will be displayed.
        Simply select with the mouse or keys the chosen room to join.
        When you click the button, you will be placed in the room, if no error occurred.
        
        When you are inside the chat, the big box is where the text from you, other clients, and server will be displayed.
        After the box is full of text you can use the arrows on the right side to scroll the text.
        The box at the bottom is the message entry field. Type your message there and press enter to send it to the server.
        When you are done with the application, just send a "/quit" message to the server.
        Be careful with your actions, because you may be banned!!
