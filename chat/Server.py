# For sockets
import socket as dobby
# For threads
import thread as wool
# For Room.getClients
from operator import concat
# For system('clear')
from os import system
# For Message format
from Message import Message

# Socket to bind.
iupi = None

# Rooms
room = None

# Server Name
serverName = None

# Banned IP's
banned = []

# Client
class Client:
    def __init__(self, sock, add, name):
        self.sock       = sock
        self.add        = add
        self.name       = name
    
        self.__repr__   = self.__str__
    
    def __str__(self):
        return str( (self.add, self.name) )
    
    def close(self):
        self.sock.close()

# Rooms
class Room:
    def __init__(self):
        self.room = {}
        self.buff = {}
        self.mutx = {}
        self.qty = 0
    
    #How many rooms
    def getQty(self):
        return self.qty
    
    def getRoomNames(self):
        return self.room.keys()
    
    #All clients
    def getClients(self):
        return reduce (
            concat,
            [
                [
                    (room_name, client)
                    for client
                    in clients
                ]
                for (room_name, clients)
                in self.room.items()
            ],
            []
        )
    
    #Checks rooms existence
    def existsRoom(self, name):
        return name in self.room
    #To prevent doubling nicks
    def existsNickInRoom(self, nick, name):
        print [client.name for client in self.room[name]], '----', name
        return name in [client.name for client in self.room[name]]
    
    #Open a new room
    def addRoom(self, name):
        if room in self.room:
            return False
        else:
            self.qty += 1
            self.room[name] = []
            self.buff[name] = []
            self.mutx[name] = wool.allocate_lock()
            return True

    #Puts a client in a room
    def addClient(self, room, client):
        if room not in self.room:
            print 'Room doesn\'t exist.'
        else:
            self.room[room].append( client )

    #Deletes a room
    def subRoom(self, name):
        for client in self.room[name]:
            client.close()
        
        self.qty -= 1
        
        del self.room[name]
        del self.buff[name]
        
    #Takes a client out of a room
    def subClient(self, room, name):
        #If only argument is name, recall this function after finding the room
        if room == None:
            for room_name in self.getRoomNames():
                self.subClient(room_name, name)
        #Take the clint out of it
        else:
            client = [client for client in self.room[room]
                             if client.name == name][0]
            
            self.room[room] = [client for client in self.room[room]
                                      if client.name != name]
            
            # client.close()
        
    #Puts a message on the room queue
    def pushMsg(self, room, msg):
        self.mutx[room].acquire()
        
        self.buff[room].append( msg )
        
        self.mutx[room].release()
    
    #Gets eventual messages from clients
    def popMsg(self, room):
        self.mutx[room].acquire()
        
        if room not in self.room:
            ans = False
        else:
            #If there are messages in the queue, send first to clients 
            if self.buff[room] != []:
                for client in self.room[room]:
                    client.sock.send( self.buff[room][0] )
                
                print self.buff[room][0]
                
                del self.buff[room][0]
            ans = True

        self.mutx[room].release()
        
        return ans
    
    def show(self): 
        print( 'There are ' + str(self.qty) + ' rooms.' )
        for room in self.room:
            print( 'Room ' + room + ':' )
            for client in self.room[room]:
                print( '   Client: ' + str(client) )
                
    
    #Deletes all the rooms
    def close(self):
        names = [name for name in self.room]

        for name in names:
            self.mutx[name].acquire()
            
            room.subRoom(name)
            
            self.mutx[name].release()


def init():
    global iupi, room, serverName

    # Server name is disable in this version of Chat'O.
    serverName = "None" #raw_input( 'Enter server name: ' )

    # Initialize socket
    iupi = dobby.socket(dobby.AF_INET, dobby.SOCK_STREAM)
    
    bound = False
    
    while not bound:
        try:
            port = int( raw_input( 'Enter port to bind: ' ) )
            if port < 1:
                raise ValueError('Port should be a positive integer')
            iupi.bind( ('', port) )

            print 'Bound to port ' + str(port)
            bound = True
        except ValueError as e:
            print 'Invalid input: ' + str(e)
        except dobby.error as e:
            print 'Could not bind to port ' + str(port)
            print str(e)
        except:
            print 'Could not bind to port ' + str(port)
            print 'Unknown error'
    
    # Initialize room
    room = Room()

def deinit():
    global iupi, room

    iupi.close()
    
    room.close()

#Keeps a connection with a client, getting messages from it
def clientConnection(rona, client):
    doneYet = False
    while not doneYet:
        try:
            msg = client.sock.recv(1024)
        except dobby.error:
            return
        
        if msg == '/quit':
            doneYet = True
            room.subClient( rona, client.name )
        else:
            room.pushMsg( rona, msg )
           
    client.sock.send('/quit')
    client.close()

def roomConnection(name):
    while room.popMsg( name ):
        pass
    
    del room.mutx[name]

#Send rooms list
def sendRooms(cnx):
    cnx.send( str(room.getQty()) )
    cnx.recv(1024) # OK
    
    for name in room.getRoomNames():
        cnx.send( name )
        cnx.recv(1024) # OK

#Sets all the main properties of the connection to clients, and start the feed
def jujuba(cnx, add):
    if add[0] in banned:
        print 'hello?'
        cnx.send('thou art not welcome in this place!')
        cnx.close()
        return
    else:
        cnx.send('welcome')
        cnx.recv(1024)

    print 'NOT BANNED'

    cnx.send( serverName )
    cnx.recv(1024) # OK
    
    #Rooms list
    sendRooms(cnx)

    nicked = False

    nick = cnx.recv(1024)
    cnx.send('ok')
    print 'NICK:', nick

    while not nicked:

        roomed = False
    
        while not roomed:
            rona = cnx.recv(1024)
            print 'ROOM:', rona
        
            if room.existsRoom(rona):
                cnx.send('ok')
                roomed = True
            else:
                cnx.send('no')
    
        
        print cnx.recv(1024)

        if room.existsNickInRoom(nick, rona):
            cnx.send('nick no')
        else:
            cnx.send('nick ok')
            nicked = True
        
    print nick + ', ' + rona
    
    client = Client(cnx, add, nick)
    
    room.addClient( rona, client )
    
    wool.start_new_thread( clientConnection, (rona, client) )

    

# Thread that waits for clients to connect.
def waitForIt():
    global iupi, serverName
    
    while True:
        iupi.listen(1)
        
        cnx, add = iupi.accept()
        
        print 'ADDRESS TRYING TO CONNECT:', add
        
        wool.start_new_thread( jujuba, (cnx, add) )

def createRoom():
    print '---------------------Create Room--------------------'
    name = raw_input('Enter room name: ')
    while not room.addRoom(name):
        print 'Room already exists.'
        name = raw_input('Enter room name: ')

    wool.start_new_thread( roomConnection, (name, ) )
    
    print 'Room succesfully created. (Hopefully)'
    pressEnterToContinue()

def banIP():
    ip = raw_input('Enter IP to ban: ')
    
    # Add to banned IP list.
    banned.append(ip)

    # Search if IP is already connected.
    # TODO
    
    for room_name, client in room.getClients():
        if client.add[0] == ip:
            pass
            client.sock.send('/ban')
            room.subClient(room_name, client.name)
            client.sock.close()

def unbanIP():
    global banned
    
    ip = raw_input('Enter IP to unban: ')
    
    banned = [i for i in banned if i != ip]

def bannedIP():
    print 'Banned IP\'s:'
    for ip in banned:
        print ip

def clearScreen():
    return system('clear')

def header():
    print '--------------------Chat\'O Server-------------------'
    # print 'You are the admin.'

def joke1():
    print 'not in the m00d right now... sry...'

def menu():
    print '1 - Create room'
    print '2 - Print joke'
    print '3 - Rooms status'
    print '4 - Ban IP'
    print '5 - Unban IP'
    print '6 - Banned IP\'s'
    print '7 - Exit'
    # print 'x - Print joke!'

def pressEnterToContinue():
    print 'Press enter to continue!'
    raw_input()

def aHasTooth():
    clearScreen()
    print 'Welcome to this Boring Server, where you will host Chat\'O rooms!!!'
    print ''
    
    init()
    wool.start_new_thread( waitForIt, () )
    pressEnterToContinue()

    doneYet = False
    
    while not doneYet:
        clearScreen()
        header()
        menu()
        
        opt = raw_input('Option: ')
        
        clearScreen()
        if   opt == '1':
            createRoom()
        elif opt == '2':
            print joke1()
            pressEnterToContinue()
        elif opt == '3':
            room.show()
            pressEnterToContinue()
        elif opt == '4':
            banIP()
            pressEnterToContinue()
        elif opt == '5':
            unbanIP()
        elif opt == '6':
            bannedIP()
            pressEnterToContinue()
        elif opt == '7':
            # TODO close rooms, kill threads, fuck it all
            print 'Bye bye!'
            doneYet = True
        else:
            print 'Invalid option.'
            pressEnterToContinue()

    deinit()

aHasTooth()

