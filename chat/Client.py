# For sockets
import socket as dobby
# For threads
import thread as wool
# For system('clear')
from os import system
# For sleep (TEST?)
from time import sleep
# For GUI
from GUI import Client as GUI
# For Message format
from Message import Message

class Client:
    def __init__(self):
        self.iupi = None
        self.nick = None
        self.host = None
        self.port = None
        self.inRoom = False
        self.gui = GUI(self)
        self.server_name = 'None'
        self.connected = False

    def clearScreen(self):
        return system('clear')
    
    #Gets available rooms and put into rooms list
    def getRooms(self):
        roomQty = int( self.iupi.recv(1024) )

        self.iupi.send('ok')
        
        print 'QTY::::', roomQty
        
        
        #Puts in the list
        for i in range(roomQty):
            room = self.iupi.recv(1024)
            self.iupi.send('ok')
            self.gui.selectScreen.insertRoom(room)
    
    #Tries to connect to server
    def connect(self):
        self.iupi = dobby.socket(dobby.AF_INET, dobby.SOCK_STREAM)
        
        
        print (self.host, self.port)
    
        try:
            self.iupi.connect( (self.host, self.port) )

            canIComeIn = self.iupi.recv(1024)
            
            print canIComeIn
            
            #IP is banned
            if canIComeIn != 'welcome':
                self.gui.showInfoWindow('You are probably banned in this server... Sry!')
                return False

            self.iupi.send('ok')

            print 'NOT BANNED'
            
            #Gets server name
            self.server_name = self.iupi.recv(1024)
            self.iupi.send('ok')

            print 'Connected to server ' + self.server_name + '!'
            
            #Error handlers
            return True
        except dobby.error as e:
            self.gui.showInfoWindow('Could not connect to server\n'+str(e))
            return False
        except:
            self.gui.showInfoWindow('Could not connect to server\nUnknown error')
            return False
    
    #Tells the server what's your name
    def roomSelection(self):
        self.getRooms()
        
        self.iupi.send(self.nick)
        self.iupi.recv(1024) # ok
    
    #Tries to join the chosen room
    def selectRoom(self, room):        
        self.iupi.send(room)
        ans = self.iupi.recv(1024)
        
        #Error
        if ans != 'ok':
            print 'This room does not exist!'
            return False
        
        #Can join
        self.iupi.send('room ok')
        ans = self.iupi.recv(1024)
        
        #Nick conflict
        if ans != 'nick ok':
            print 'There is already somebody in this room with the nick', nick, ':/'
            return False
        
        #Starts the thread of the message feed from the server
        wool.start_new_thread( self.roomThread, () )

        return True
    
    #Closes connection
    def disconnect(self):
        return
        self.iupi.close()
    
    #Gets feed of messages from the server
    def roomThread(self):
        self.inRoom = True
        while self.inRoom:
            msg = self.iupi.recv(1024)
            print 'oie'
            #Connection is going to close
            if msg == '/quit':
                self.inRoom = False
            #Connection will be closed and you will be banned
            elif msg == '/ban':
                self.inRoom = False
                self.gui.showInfoWindow('Thou art not welcome here!')
                self.gui.putMsg (
                    Message( 'server', 'Thou art not welcome here!' ).toStr()
                )
                self.gui.putMsg (
                    Message( 'server', 'Input anything to end chat...' ).toStr()
                )
            else:
                self.gui.putMsg( msg )
            
            print 'Received msg:', msg
        
        print 'EXIT'
        self.iupi.close()
    
    #Gets GUI input and sends to server
    def chat(self):
        print 'Entrou no chat'
        while self.inRoom:
            msg = Message(self.nick, self.gui.getMsg())
            
            print msg.toStr()
            
            #You want to quit and will be disconnected
            if msg.msg == '/quit':
                self.inRoom = False
                msg = '/quit'
            else:
                msg = msg.toStr()
            
            #Send message to server
            try:
                self.iupi.send( msg )
            except:
                pass
        
        print 'HEY YOU, BOY!'
        self.gui.readyToClose()
    
    #When chat screen is started on GUI, sets connection to be ready to exchange messages with the server.
    def aHasTooth(self):
        self.nick, self.host, self.port = self.gui.getINFO()

        self.clearScreen()
        print 'Welcome to this Boring Client, where you will enter Chat\'O rooms!!!'
        print ''
        
        #self.connect()

        self.chat()
        
        # Wait for iupi to close. TODO delete it.
        sleep(1)
        self.gui.close()

    def start(self):
        self.gui.initMenu()
    	
    
if __name__ == '__main__':
    Client().start()

