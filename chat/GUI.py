# For GUI
from Tkinter import *
# For alert message windows
import tkMessageBox
# For queuing messages
from Queue import Queue
# For threads
import thread as wool
# For Message format
from Message import Message

class Client:
    #Create the screen and set it up
    def __init__(self, jujuba):
        self.root = Tk()
        self.root.title("Chat'O")
        self.jujuba = jujuba

        self.root.protocol('WM_DELETE_WINDOW', self.onClosing)
        self.closing = False
    
    #Request to close connection
    def onClosing(self):
        try:
            if self.jujuba.inRoom:
                self.buf_user.put( Message(msg='/quit') )
            
                # FODA-SE: GAMBIARRA FTW
                self.jujuba.iupi.send('/quit')
                self.jujuba.inRoom = False
                self.jujuba.iupi.close()
        finally:
            self.root.destroy()
    
    def readyToClose(self):
        self.closing = False
        
    #Sets main window to menu
    def initMenu(self):
        self.menuScreen = StartScreen(self.root,self)

        self.root.mainloop()
    #Sets main window to room selection
    def initRoomSelection(self):
        self.root.title("Chat'O - " + self.nick)
        self.selectScreen = RoomSelection(self.root,self,self.jujuba)
        self.jujuba.roomSelection()
    
    #Set infos input from menu
    def setINFO(self,nick,host,port):
        self.nick, self.host, self.port = nick, host, int(port)
        self.jujuba.nick, self.jujuba.host, self.jujuba.port = nick, host, int(port)
    def getINFO(self):
        return self.nick, self.host, self.port
    
    #Gives alert message for being banned
    def showInfoWindow(self,message):
        tkMessageBox.showinfo("Server Info", message)
    
    #Sets main window to chat
    def initChat(self):
        self.buf_user = Queue()
        self.buf_server = Queue()
        
        self.textHistory = []
        self.userHistory = []
        
        #Set scrollbar to text
        self.S = Scrollbar(self.root)
        self.T = Text(self.root, height=15, width=50)
        self.S.grid(row= 0, column= 1)
        self.T.grid(row= 0,column= 0)
        self.S.config(command=self.T.yview)
        self.T.config(yscrollcommand=self.S.set)
        self.T.config(state=DISABLED)
        
        #Sets chat message input
        self.entry = Entry(self.root)
        self.entry.grid(row= 1, columnspan=2)
        self.handleEntry()
        
        #Start server connection settings to trigger feed on
        self.chatOutput()
        wool.start_new_thread( self.jujuba.aHasTooth, () )
    
    #Places a message on the chat widget
    def addMsg(self,user,msg):
        self.T.config(state=NORMAL)
        self.userHistory.append(user)
        self.textHistory.append(msg)
        self.T.insert(END,user + ': ' + msg + '\n')
        self.T.config(state=DISABLED)
    
    #Binds enter key to input
    def handleEntry(self):
        
        self.entryMsg = StringVar()
        
        self.entryMsg.set("")
        
        self.entry["textvariable"] = self.entryMsg

        
        self.entry.bind('<Key-Return>', self.chatInput)
    
    #Stores what is in the input and clears entry field
    def chatInput(self, event):
        msg = self.entryMsg.get()
        self.entryMsg.set("")
        
        self.buf_user.put(msg)
    
    #Keeps retrieving messages from the server queue
    def chatOutput(self):
        if not self.buf_server.empty():
            msg = Message(string = self.buf_server.get())
            self.addMsg(msg.user, msg.msg)
        
        self.root.after(100, self.chatOutput)
    
    #Handles messages from the server
    def getMsg(self):
        if self.closing:
            return '/quit'
        else:
            return self.buf_user.get()        
    
    #Gets message from server to be handled
    def putMsg(self, msg):
        self.buf_server.put(msg)
    
    def close(self):
        self.root.quit()

#Defines the room selection screen properties
class RoomSelection(object):
    def __init__(self, root, client,jujuba):
        self.jujuba = jujuba
        self.root = root
        self.client = client
        
        self.roomsTitle = Label(self.root,text = 'Choose from the available rooms:')
        self.roomsTitle.grid(row= 0)
        
        #Creates list of available rooms
        self.roomsList = Listbox(self.root, height= 10)
        self.roomsList.grid(row= 1)
        
        #Button for selection
        self.button = Button(self.root,text = 'Join!')
        self.button['command'] = self.tryToJoin
        self.button.grid(row=2)

    #Finishes the room selection screen
    def destroySelect(self):
        self.roomsTitle.grid_remove()
        self.roomsList.grid_remove()
        self.button.grid_remove()
    
    #At the end, tells the server the chosen room
    def tryToJoin(self):
        self.destroySelect()
        self.jujuba.selectRoom( self.roomsList.get( ACTIVE ) )
        self.client.initChat()

    #Puts a room into the rooms list
    def insertRoom(self, room):
        self.roomsList.insert(END, room)

#Class for dealing with entry fields
class GuiEntry:
    def __init__(self, root):
        self.entry = Entry(root)
        
        self.entryMsg = StringVar()
        self.entryMsg.set("")
        
        self.entry["textvariable"] = self.entryMsg
        
    def get(self):
        return self.entryMsg.get()
        
    def grid_remove(self):
        self.entry.grid_remove()

#Defines the menu screen properties
class StartScreen:
    def __init__(self,root,client):
        
        self.root = root
        self.client = client
        
        self.title = Label(self.root, height=2, width=30, text='Welcome to Boring Client! \nPlease input the following')
        self.title.grid(row= 0, columnspan= 2)
        
        #Nick Entry
        self.nick_label = Label(self.root, height=1, width=10,text= 'Nickname:')
        self.nick_label.grid(row= 1, column = 0)
        self.nickEntry = GuiEntry(self.root)
        self.nickEntry.entry.grid(row= 1, column= 1)
        
        #Host entry
        self.host_label = Label(self.root, height=1, width=10,text= 'Host IP:')
        self.host_label.grid(row= 2, column = 0)
        self.hostEntry = GuiEntry(self.root)
        self.hostEntry.entry.grid(row= 2, column= 1)
        
        #Port entry
        self.port_label = Label(self.root, height=1, width=10,text= 'Host Port:')
        self.port_label.grid(row= 3, column = 0)
        self.portEntry = GuiEntry(self.root)
        self.portEntry.entry.grid(row= 3, column= 1)
        
        self.startButton = Button(self.root,text = 'Start chat!')
        self.startButton["command"] = self.tryToConnect
        self.startButton.grid(row= 4, columnspan= 2)
    
    #Gets input info and tries to connect to server.
    def tryToConnect(self):
        if self.nickEntry.get() == '':
            self.client.showInfoWindow('Enter nickname!!!')
            return
        
        if not self.portEntry.get().isdigit():
            self.client.showInfoWindow ('Port must be only digits!!!')
            return

        self.client.setINFO(self.nickEntry.get(),self.hostEntry.get(),self.portEntry.get())
        
        #Here it tries to connect and change screen
        if self.client.jujuba.connect():
            self.destroyMenu()
            self.client.initRoomSelection()
    
    
    def destroyMenu(self):
        self.title.grid_remove()
        self.nick_label.grid_remove()
        self.nickEntry.grid_remove()
        self.host_label.grid_remove()
        self.hostEntry.grid_remove()
        self.port_label.grid_remove()
        self.portEntry.grid_remove()
        self.startButton.grid_remove()
        
    
if __name__ == '__main__':
    
    cp = Client()
    cp.initMenu()
    # cp.initChat()
    
