class Message:
    def __init__(self, user = 'no one', msg = 'Jujuba', string = None): # date...
        if string == None:
            self.user = user
            self.msg  = msg
        else:
            self.user, self.msg = eval(string)
    
    def toStr(self):
        return str( (self.user, self.msg) )
    
if __name__ == "__main__":
    m = Message('vtcb', 'lalalala')
    
    print m.toStr()
    
    
    m = Message(string='(\'lmpsl\', \'lulululu\')')
    
    print m.toStr()
    
    m = Message(msg='/quit')
    
    print m.toStr()
    
    m = Message('server', 'ban msg')
    
